## Общение: митапы, стримы, форумы, чаты

- [YouTube-канал #RuPostgres](https://youtube.com/RuPostgres/) – онлайны, Постгрес-вторники
- [Большая коллекция видео о Postgres на русском](https://www.youtube.com/RuPostgres/playlists)
- [Facebook-группа #RuPostgres](https://www.facebook.com/groups/RuPostgres/)
- [Facebook-группа "PostgreSQL в России"](https://www.facebook.com/groups/PostgreSQL/)
- [Телеграм-группа pgsql](https://t.me/pgsql)
- [Facebook-группа PostgreSQL Belarus](https://www.facebook.com/groups/postgresby/)
- [Телеграм-канал PostgreSQL Belarus](https://t.me/postgresby)
- [Форум на SQL.ru](https://www.sql.ru/forum/postgresql)
- <s>[Meetup-группа #RuPostgres](https://www.meetup.com/postgresqlrussia/) – онлайн-встречи, митапы</s> (закрыта)
- [Митапы PostgresPro](https://postgrespro.timepad.ru/events/)


## Документация

- [Параметры конфигурации postgresqlco.nf](https://postgresqlco.nf/ru/doc/param/)
- [Перевод документации на русский (PostgresPro)](https://postgrespro.ru/docs/postgresql)
